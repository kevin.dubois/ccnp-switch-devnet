# Import Netmiko connection handler
from netmiko import ConnectHandler
from ipaddress import IPv4Address # Formater des adresses IP
from getpass import getpass # Demander un mot de passe
from pprint import pprint # Pretty print

import threading

switches = [IPv4Address("10.0.20.1"), IPv4Address("10.0.20.2"), IPv4Address("10.0.20.3")]


USERNAME = "cisco"
PASSWORD = getpass(prompt="Password SSH:")

def configure_vlan(switch, commands):
    td = threading.local()
    print("Configure switch {ip}".format(ip=switch))
    td.ssh = ConnectHandler(host=str(switch), username=USERNAME, password=PASSWORD, device_type="cisco_ios")

    td.ssh.send_config_set(commands)
    print("Finished configuring switch {ip}".format(ip=switch))


# Create commands to push to switches
commands = []

for vlan in range(100,120):
    commands.append("vlan {id}".format(id=vlan))
   
thread_pool = []

for switch in switches:
    t = threading.Thread(target=configure_vlan, args=(switch, commands))
    t.start()
    thread_pool.append(t)

for t in thread_pool:
    t.join()
    
# Import Netmiko connection handler
from netmiko import ConnectHandler
from ipaddress import IPv4Address # Formater des adresses IP
from getpass import getpass # Demander un mot de passe
from pprint import pprint # Pretty print

switches = [IPv4Address("10.0.20.1"), IPv4Address("10.0.20.2"), IPv4Address("10.0.20.3")]

vlans = [
    { "name": "Users", "id": 100 },
    { "name": "Voice", "id": 110 },
    { "name": "Printers", "id": 120 }
]

username = "cisco"
password = getpass(prompt="Password SSH:")

commands = []

for vlan in vlans:
    commands.append("vlan {id}".format(id=vlan['id']))
    commands.append("name {name}".format(name=vlan['name']))

for switch in switches:
    ssh_connection = ConnectHandler(host=str(switch), username=username, password=password, device_type="cisco_ios")

    name = ssh_connection.find_prompt().rstrip('#')
    print("Configure VLANs on switch {name}".format(name=name))

    ssh_connection.send_config_set(commands)
    print("Finished with switch {name}".format(name=name))
